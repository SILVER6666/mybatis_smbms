package cn.smbms.service.role.impl;

import java.util.List;

import javax.annotation.Resource;


import cn.smbms.dao.role.RoleMapper;
import cn.smbms.pojo.Role;
import cn.smbms.service.role.RoleService;
public class RoleServiceImpl implements RoleService {

	@Resource
	private RoleMapper roleMapper;
	
	@Override
	public List<Role> getRoleList() {
		List<Role> roleList=null;
		try {
			roleList=roleMapper.getRoleList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return roleList;
	}

}
